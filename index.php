<?php

// include minnelli
require_once 'minnelli.php';

// configure minnelli
config(array());

// add some routes...
get('/', function() {
	view('home', array());
});

get('/api', function() {
	echo 'api get';
});

put('/api', function() {
	echo 'api put';
});

// run minnelli, run
run();

// EOF