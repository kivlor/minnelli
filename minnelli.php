<?php

class M
{
	// config
	private static $config = array(
		'views' => 'views/',
		'layout' => 'layout',
	);

	// request method routes
	private static $get = array();
	private static $post = array();
	private static $put = array();
	private static $patch = array();
	private static $delete = array();

	// request params
	private static $params = array();

	/***************************************
	*
	*	Boot methods?
	*
	***************************************/

	public static function config($config = array())
	{
		if(!is_array($config))
		{
			exit('config : config must be an array');
		}

		M::$config = array_merge(self::$config, $config);

		return;
	}

	/***************************************
	*
	*	Route method...
	*
	***************************************/

	public static function route($type, $route, $callback)
	{
		// make sure type is a valid attr
		if(!isset(self::$$type))
		{
			self::halt(500);
		}

		// have to assign self::$$type by ref to $routes to manipulate
		// self::$$type[$route] = $callback throws an illegal string offset??
		$routes = &self::$$type;
		$routes[$route] = $callback;

		return;
	}

	/***************************************
	*
	*	Parameter Methods
	*
	***************************************/

	public static function params()
	{
		return M::$params;
	}

	public static function param($name)
	{
		return isset(M::$params[$name]) ? M::clean(M::$params[$name]) : false;
	}

	private static function clean($value)
	{
		return trim(strip_tags($value));
	}

	/***************************************
	*
	*	View methods
	*
	***************************************/

	public static function view($view, $data, $return = false, $use_layout = true)
	{
		// set full path
		$view = substr($view, 0, -4) === '.php' ? $view : "{$view}.php";
		$view = M::$config['views'].$view;

		// does file exist?
		if(!is_file($view) || !is_readable($view))
		{
			halt(500);
		}

		// extract data
		if(is_array($data) && !empty($data))
		{
			extract($data, EXTR_OVERWRITE);
		}

		// are we using the layout file?
		if($use_layout)
		{
			// get layout file
			$layout = substr($view, 0, -4) === '.php' ? M::$config['layout'] : M::$config['layout'].'.php';
			$layout = M::$config['views'].$layout;

			// does layout exist?
			if(!is_file($layout) || !is_readable($layout))
			{
				halt(500);
			}

			// stringify the view
			ob_start();
			include $view;
			$yield = ob_get_clean();

			// return layout
			if($return)
			{
				ob_start();
				include $layout;
				return ob_get_clean();
			}
			// otherwise show the layout
			else
			{
				include $layout;
				exit(); // view is final if not returning
			}
		}
		// ok, then just show (or return) the view
		else
		{
			// return view
			if($return)
			{
				ob_start();
				include $view;
				return ob_get_clean();
			}
			// otherwise show the layout
			else
			{
				include $view;
				exit(); // view is final if not returning
			}
		}	

		return;
	}

	/***************************************
	*
	*	Run!
	*
	***************************************/

	public static function run()
	{
		// get path info
		$path = $_SERVER['REQUEST_URI'];

		// let's see if the path exists in one or the request types

		// detect request type, TODO ;)
		$type = isset($_SERVER['REQUEST_METHOD']) ? strtolower($_SERVER['REQUEST_METHOD']) : 'get'; // default to get if server doesn't contain request method...
		$routes = &self::$$type;

		if(!isset($routes[$path]))
		{
			// hmmm, welp, 404 I guess
			self::halt(404);
		}
		
		// set params
		if($type !== 'get')
		{
			switch($type)
			{
				case 'post':
					M::$params = $_POST;
					break;

				default:
					parse_str(file_get_contents("php://input"), M::$params);
					break;
			}
		}

		call_user_func($routes[$path]);
		exit();

		return;
	}

	public static function redirect($location)
	{
		header("Location: {$location}");
		exit();
	}

	public static function halt($status = 404, $message = null)
	{
		// a very basic status map
		$status_codes = array(
			200 => 'OK',

			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',

			500 => 'Internal Server Error',
		);

		// if the status sent doesn't exist use 400
		if(!isset($status_codes[$status]))
		{
			$status = 400;
		}

		// set the meesage to $status + $status_code[$status] when empty
		if(empty($message))
		{
			$message = "{$status} - {$status_codes[$status]}";
		}

		// set the header
		header("HTTP/1.0 {$message}}");

		// then exit with the message
		exit($message);

		return;
	}
}

// wrapper functions for the major methods of minnelli
// maybe I could use some magic to call these without having to decare the function??

function config($config){ return M::config($config); }

function get($route, $callback){ return M::route('get', $route, $callback); }
function post($route, $callback){ return M::route('post', $route, $callback); }
function put($route, $callback){ return M::route('put', $route, $callback); }
function patch($route, $callback){ return M::route('patch', $route, $callback); }
function delete($route, $callback){ return M::route('delete', $route, $callback); }

function params(){ return M::params(); }
function param($name){ return M::param($name); }

function view(){ $args = func_get_args(); return call_user_func_array('M::view', $args); }
function halt(){ $args = func_get_args(); return call_user_func_array('M::halt', $args); }
function redirect(){ $args = func_get_args(); return call_user_func_array('M::redirect', $args); }

function run(){ return M::run(); }

// EOF